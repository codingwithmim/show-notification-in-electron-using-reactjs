const { app, Notification, BrowserWindow } = require("electron");

const path = require("path");
const isDev = require("electron-is-dev");

const ipc = require('electron').ipcMain

let mainWindow;

function createWindow() {
  mainWindow = new BrowserWindow({ width: 900, height: 680, webPreferences: { nodeIntegration: true } });
  mainWindow.loadURL(
    isDev
      ? "http://localhost:3000"
      : `file://${path.join(__dirname, "../build/index.html")}`
  );
  mainWindow.on("closed", () => (mainWindow = null));
}

app.whenReady().then(createWindow).then(showNotification)

app.on("window-all-closed", () => {
  if (process.platform !== "darwin") {
    app.quit();
  }
});

app.on("activate", () => {
  if (mainWindow === null) {
    createWindow();
    notificationMethod();
  }
});
function showNotification() {
  const notification = {
    title: 'Basic Notification',
    body: 'Notification from the Main process'
  }
  new Notification(notification).show()
}
const notificationMethod = () => {
  showNotification()
}

ipc.on('notification', (event, arg) => {
  console.log("notification ", event, arg);
  showNotification()
})
